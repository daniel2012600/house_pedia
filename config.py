# -*- coding: utf-8 -*-
class Config(object):

    SQL_CONFIG = {
        'host': 'localhost',
        'port':3306,
        'connect_timeout': 60,
        'read_timeout': 60,
        'write_timeout': 60,
        'max_allowed_packet': 102400,
        'user': 'root',
        'password': 'root',
        'db': 'rent_info',
        'charset': 'utf8'
    }

