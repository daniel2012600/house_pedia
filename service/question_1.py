import pymysql
from config import Config as cfg
from flask_restful import reqparse
from flask_restful import Resource 

class solve_1(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('city', required=False, help='', location='json')
    parser.add_argument('sex', required=False, help='', location='json')

    def _exec_sql(self, sql):
        conn = pymysql.connect(**cfg.SQL_CONFIG)
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute(sql)

        item = cursor.fetchall()

        cursor.close()
        conn.close()

        return item

    def get(self):

        sql = f'''
        SELECT * FROM rent_info.`rent_table`
        LIMIT 100;
        '''

        return self._exec_sql(sql)

    def post(self):
        # 驗證欲查詢之條件
        arg = self.parser.parse_args()
        city = arg['city']
        sex = arg['sex']

        #   如果查詢值為null 則回傳空值 , 有則
        #         WHERE city = '台北市' AND host_host_name LIKE '吳小姐';

        sql = f'''
            WITH want_data AS (
                SELECT *
                FROM rent_info.`rent_table`
                WHERE city LIKE '{city}'
                AND require_sex LIKE '{sex}'
                OR require_sex = '男女生皆可'
            ) 
            
            SELECT addr , price , host_name, phone_number, current_situation, require_sex
            FROM  want_data
            LIMIT 50

        '''
        result = [{
            'xxx' : city,
            'table': self._exec_sql(sql)
        }]

        return result

    def put(self):
        pass

    def delete(self):
        pass
