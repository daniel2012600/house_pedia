import pymysql
from config import Config as cfg
from flask_restful import reqparse
from flask_restful import Resource 

class solve_2(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('phone_number', required=False, help='', location='json')

    def _exec_sql(self, sql):
        conn = pymysql.connect(**cfg.SQL_CONFIG)
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute(sql)

        item = cursor.fetchall()

        cursor.close()
        conn.close()

        return item

    def get(self):

        sql = f'''
        SELECT * FROM rent_info.`rent_table`
        LIMIT 100;
        '''

        return self._exec_sql(sql)

    def post(self):
        # 驗證欲查詢之條件
        arg = self.parser.parse_args()
        phone_number = arg['phone_number']

        #   如果查詢值為null 則回傳空值 , 有則
        #         WHERE city = '台北市' AND host_host_name LIKE '吳小姐';

        sql = f'''
            WITH want_data AS (
                SELECT *
                FROM rent_info.`rent_table`
                WHERE 
                phone_number  LIKE '%{phone_number}%'
            ) 
            
            SELECT *
            FROM  want_data
            LIMIT 10

        '''

        return self._exec_sql(sql)

    def put(self):
        pass

    def delete(self):
        pass
