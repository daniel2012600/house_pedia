import os
import sys
import time
import csv
import queue
import random 
import logging
import requests
import traceback
import threading
import pandas as pd
from lxml import etree
from datetime import datetime
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

CHROME_PATH = 'C:/interview_test/company_wistron/chromedriver.exe'
LOG_PATH = 'C:/interview_test/company_wistron/my_log/'
SOURCE_PATH = 'C:/interview_test/company_wistron/my_data'
FILE_DATE = datetime.today().strftime('%Y-%m-%d')

house_table ={
        "city":[],
        "addr":[],
        "price":[],
        "host_name":[],
        "host_rule":[],
        "phone_number":[],
        "house_type":[],
        "current_situation":[],
        "require_sex":[]
        }

# Worker 類別，負責處理資料
class Worker(threading.Thread):
    def __init__(self, queue, num, city):
        threading.Thread.__init__(self)
        self.queue = queue
        self.num = num

    # input 物件網址 撈取網頁資料 return 欄位 
    def getData(self, url, area):
        user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36'
        headers = {'User-Agent': user_agent}    
        request_url='https:'+str(url).strip()
        res = requests.get(request_url, headers=headers)
        if res.status_code == 200:
            house_content = etree.HTML(res.text)
            city = area
            addr = 'NULL'
            price = 'NULL'
            host_name = 'NULL'
            host_rule = 'NULL'
            house_type = 'NULL'
            current_situation = 'NULL'
            require_sex = 'NULL'

            addr = ''.join(house_content.xpath("//div/span[@class='addr']/text()"))
            price= ''.join(house_content.xpath("//div[@class='detailInfo clearfix']/div[@class='price clearfix']/i/text()"))
            price = ''.join(price.split(','))
            host_name = house_content.xpath("//div[@class='avatarRight']/div/i/text()")[0]
            role_list = ['屋主', '仲介', '代理人']
            host_rule = house_content.xpath("//div[@class='avatarRight']/div/text()")[0].strip('（').strip('）') 
            for i in role_list:
                if i in host_rule:
                    host_rule = i

            phone_element = "//div[@class='hidtel']/text()"
            phone_number = house_content.xpath(phone_element) if house_content.xpath(phone_element) else house_content.xpath("//span[@class='dialPhoneNum']/@data-value")
            house_type = ''.join(house_content.xpath("//div[@class='detailInfo clearfix']/ul/li[contains(.,'型態')]/text()")).split('\xa0')[-1] 
            current_situation =''.join(house_content.xpath("//div[@class='detailInfo clearfix']/ul/li[contains(.,'現況')]/text()")).split('\xa0')[-1] 
            sex = ''.join(house_content.xpath("//div[@class='one'][contains(.,'性別要求')]/../div[@class='two']/em/text()"))
            require_sex = sex if  sex  else "男女生皆可"
                        
            return city, addr, int(price), host_name, host_rule, phone_number[0], house_type, current_situation, require_sex
        else:
            print('link expired:', url)
            return 404, 404, 404, 404, 404, 404, 404
    def run(self):
        while self.queue.qsize() > 0:
            # 取得新的資料
            data_area = self.queue.get()
            data = self.queue.get()
            # 處理資料
            city, addr, price, host_name, host_rule, phone_number, house_type, current_situation, require_sex = self.getData(data)
            house_table['city'].append(city)
            house_table['addr'].append(addr)
            house_table['price'].append(price)
            house_table["host_name"].append(host_name)
            house_table["host_rule"].append(host_rule)
            house_table["phone_number"].append(phone_number)
            house_table["house_type"].append(house_type)
            house_table["current_situation"].append(current_situation)
            house_table["require_sex"].append(require_sex)
            time.sleep(random.randint(1,3))

def check_loading(driver):
    while True:
        check = driver.execute_script('return document.readyState;')
        time.sleep(2)
        if check == 'complete':
            break

# =======================偵錯方法=============================
def _error_msg(e):
    error_class = e.__class__.__name__ #取得錯誤類型
    detail = e.args[0] #取得詳細內容
    cl, exc, tb = sys.exc_info() #取得Call Stack
    lastCallStack = traceback.extract_tb(tb)[-1] #取得Call Stack的最後一筆資料
    fileName = lastCallStack[0] #取得發生的檔案名稱
    lineNum = lastCallStack[1] #取得發生的行號
    funcName = lastCallStack[2] #取得發生的函數名稱
    errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
    return errMsg
    
def create_logger(area, date):
    # config
    filename = f'error_{area}_{date}.log'    # 設定檔名
    logging.captureWarnings(True)   # 捕捉 py waring message
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    my_logger = logging.getLogger('py.warnings')    # 捕捉 py waring message
    my_logger.setLevel(logging.ERROR)
    
    # 若不存在目錄則新建
    if not os.path.exists(LOG_PATH+date):
        os.makedirs(LOG_PATH+date)
    
    # file handler
    fileHandler = logging.FileHandler(LOG_PATH+date+'/'+filename, 'a+', 'utf-8')
    fileHandler.setFormatter(formatter)
    my_logger.addHandler(fileHandler)
    
    # console handler
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    consoleHandler.setFormatter(formatter)
    my_logger.addHandler(consoleHandler)
        
    return my_logger

def main(outputfile, area):
    try:
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        prefs = {
            'profile.default_content_setting_values':
                {
                    'notifications': 2
                }
        }
        chrome_options.add_experimental_option('prefs', prefs)  # 关掉浏览器左上角的通知提示，如上图
        chrome_options.add_argument("disable-infobars")  # 关闭'chrome正受到自动测试软件的控制'提示

        browser = webdriver.Chrome(executable_path = CHROME_PATH, chrome_options= chrome_options)
        browser.get("https://rent.591.com.tw/?kind=0&region=1")
        browser.find_element_by_id('area-box-close').click()
        check_loading(browser)
        browser.execute_script("return document.getElementsByClassName('search-location-span ')[0].click()")
        browser.find_element_by_xpath(f"//li[@class='city-li'][contains(.,'{area}')]").click()

        check_loading(browser)


        bs = BeautifulSoup(browser.page_source, 'html.parser')
        totalpages = int(bs.find('span', {'class':'TotalRecord'}).text.split(' ')[-2])/30 + 1

        for i in range(int(totalpages)):
            room_url_list=[] #存放網址list
            bs = BeautifulSoup(browser.page_source, 'html.parser')
            titles=bs.findAll('h3') # h3 放置物件的區塊
            for title in titles:
                room_url=title.find('a').get('href') # 每個物件的 url
                room_url_list.append(room_url)
                time.sleep(random.randint(1,3))

            # 多執行緒獲取資料
            try:
                my_queue = queue.Queue()
                for url in room_url_list:
                    my_queue.put(url)

                my_worker1 = Worker(my_queue, 1, area)
                my_worker2 = Worker(my_queue, 2, area)
                my_worker3 = Worker(my_queue, 3, area)

                # 讓 Worker 開始處理資料
                my_worker1.start()
                my_worker2.start()
                my_worker3.start()

                # 等待所有 Worker 結束
                my_worker1.join()
                my_worker2.join()
                my_worker3.join()
                print(i/totalpages*100, '%',end='\r') # print out 完成 %數

            except Exception as e :
                print('*' * 10)
                print(_error_msg(e))
                print('*' * 10)
            finally:
                # # ------------- write into csv ------------- #
                get_table = pd.DataFrame(house_table)
                get_table.to_csv(f'{SOURCE_PATH}/{outputfile}.csv', index = False)

            #若偵測到無法點選最後一頁則跳出
            if bs.find('a',{'class':'last'}):
                pass
            else:
                #撈取完資料後點選下一頁，並等待 3 秒載入新頁面
                browser.find_element_by_class_name('pageNext').send_keys(Keys.ESCAPE)
                browser.find_element_by_xpath("//div/a[@class='pageNext']").click()
                check_loading(browser)
            
    except Exception as e :
        logger = create_logger(area, FILE_DATE)
        logger.exception(e)


if __name__ == '__main__':
    output_file_taipei = f'taipei_{FILE_DATE}'
    output_file_name = f'new_taipei_{FILE_DATE}'
    # ---------------------------------------- #
    main(output_file_taipei,'台北市')
    main(output_file_name,'新北市')


