import os 
import time
import pymysql
import pandas as pd
from datetime import datetime
from sqlalchemy import create_engine

SQL_CONFIG = {
    'host': 'localhost',
    'port':3306,
    'connect_timeout': 60,
    'read_timeout': 60,
    'write_timeout': 60,
    'max_allowed_packet': 102400,
    'user': 'root',
    'password': 'root',
    'db': 'rent_info',
    'charset': 'utf8'
}

FILE_DATE = datetime.today().strftime('%Y-%m-%d')
FILE_PATH = 'C:/interview_test/company_wistron/my_data'

def receive_data():
    engine = create_engine('mysql+pymysql://root:root@localhost:3306/rent_info')
    all_csv_file = os.listdir(FILE_PATH) 
    for file_name in all_csv_file:
        if file_name.endswith(".csv"):
            this_file_date = file_name.split('_')[-1]
            if this_file_date.replace('.csv','') == str(FILE_DATE):
                # table_name = file_name.replace(f'_{this_file_date}','')
                pd.read_csv(f'{FILE_PATH}/{file_name}',index_col=0).to_sql(file_name.replace('.csv',''),engine,if_exists='replace')

def merge_table():

    sql = '''
        CREATE TABLE  rent_info.`rent_table` 
        AS
        (
            SELECT * FROM rent_info.taipei
            UNION ALL 
            SELECT * FROM rent_info.new_taipei
        );
        '''

    conn = pymysql.connect(**SQL_CONFIG)
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    cursor.execute(sql)

    cursor.close()
    conn.close()


receive_data()
merge_table()
