import json
import pymysql
from flask import Flask
from flask import render_template
from flask import request
from config import Config as cfg
from flask_restful import Api
from service.question_1 import solve_1
from service.question_2 import solve_2
from service.question_3 import solve_3
from service.question_4 import solve_4
from flasgger import Swagger
from flask import jsonify 
from flasgger import swag_from
import random 

app = Flask(__name__)
api = Api(app)
api.add_resource(solve_1, "/question_1/")
api.add_resource(solve_2, "/question_2/")
api.add_resource(solve_3, "/question_3/")
api.add_resource(solve_4, "/question_4/")

swagger = Swagger(app)

@app.route('/')
def hello():
    return 'Hello World!'


@app.route('/question_1/', methods=['POST'])
def question_1():
    """
    This is the Question_1 API 
    ---
    tags:
      - Question_1 API
    parameters:
      - name: parameters
        in: body
        type: string
        properties:
            city:
                type: string
                description: The city name
            sex:
                type: string
                description: The sex field
        required: true
        description: query table

    responses:
      500:
        description: Error !
      200:
        description: table_data
        schema:
            properties:
                addr:
                    type: string
                    description: The addr name
                price:
                    type: integer
                    description: The price field
                host_name:
                    type: string
                    description: The host_name field
                phone_number:
                    type: string
                    description: The host_name field
                current_situation:
                    type: string
                    description: The host_name field
                require_sex:
                    type: string
                    description: The host_name field

    """

    return jsonify(solve_1())
    
@app.route('/question_2/', methods=['POST'])
def question_2():
    """
    This is the Question_2 API 
    ---
    tags:
      - Question_2 API
    parameters:
      - name: parameters
        in: body
        required: true
        type: string
        description: query table
        properties:
            phone_number:
                type: string
                description: The phone_number
    responses:
      500:
        description: Error !
      200:
        description: table_data
        schema:
            properties:
                addr:
                    type: string
                    description: The addr name
                price:
                    type: integer
                    description: The price field
                host_name:
                    type: string
                    description: The host_name field
                phone_number:
                    type: string
                    description: The host_name field
                current_situation:
                    type: string
                    description: The host_name field
                require_sex:
                    type: string
                    description: The host_name field
    """

    return jsonify(solve_2())
    
@app.route('/question_3/', methods=['POST'])
def question_3():
    """
    This is the Question_3 API 
    ---
    tags:
      - Question_3 API
    parameters:
      - name: parameters
        in: body
        required: true
        type: string
        description: query table
        properties:
            rule:
                type: string
                description: The rule

    responses:
      500:
        description: Error !
      200:
        description: table_data
        schema:
            properties:
                addr:
                    type: string
                    description: The addr name
                price:
                    type: integer
                    description: The price field
                host_name:
                    type: string
                    description: The host_name field
                phone_number:
                    type: string
                    description: The host_name field
                current_situation:
                    type: string
                    description: The host_name field
                require_sex:
                    type: string
                    description: The host_name field
    """
    return jsonify(solve_3())

@app.route('/question_4/', methods=['POST'])
def question_4():
    """
    This is the Question_4 API 
    ---
    tags:
      - Question_4 API
    parameters:
      - name: parameters
        in: body
        required: true
        type: string
        description: query table
        properties:
            city:
                type: string
                description: The city name
            host_name:
                type: string
                description: The host_name field

    responses:
      500:
        description: Error !
      200:
        description: table_data
        schema:
            properties:
                addr:
                    type: string
                    description: The addr name
                price:
                    type: integer
                    description: The price field
                host_name:
                    type: string
                    description: The host_name field
                phone_number:
                    type: string
                    description: The host_name field
                current_situation:
                    type: string
                    description: The host_name field
                require_sex:
                    type: string
                    description: The host_name field
    """
    return jsonify(solve_4())


if __name__ == "__main__":
    app.run(host = '127.0.0.1', post = 5000 , debug=True)

